import { sleep } from 'k6';
import http from 'k6/http';

export const options = {
  duration: '5s',
  vus: 4,
  thresholds: {
    http_req_duration: ['p(95)<1'], // 95 percent of response times must be below 500ms
  },
};

export default function () {
  http.get('http://test.k6.io/contacts.php');
  sleep(3);
}

//k6 run loadtests/performance-test.js
